#!/bin/bash

#mkdir -p /etc/ssh/
#echo $SSH_HOST_DSA_KEY     > /etc/ssh/ssh_host_dsa_key
#echo $SSH_HOST_ECDSA_KEY   > /etc/ssh/ssh_host_ecdsa_key
#echo $SSH_HOST_ED25519_KEY > /etc/ssh/ssh_host_ed25519_key
#echo $SSH_HOST_RSA_KEY     > /etc/ssh/ssh_host_rsa_key

mkdir -p /root/.ssh
chmod 700 /root/.ssh
touch /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
echo ${PUBLIC_KEY} > /root/.ssh/authorized_keys

