"""an api for calculating checksums of file on S3"""
import hug
import hashlib
import requests
import zlib
from urllib.parse import urlencode

# optimal chunk size for S3 is 25-50 Mb
chunk_size = 25 * 2**20


@hug.post("/adler32", map_params={"Signature": "sig", "Expires": "exp"})
def adler32(url, sig=None, exp=None):
    """
    originally taken from Rucio: https://github.com/rucio

    An Adler-32 checksum is obtained by calculating two 16-bit checksums A and
    B and concatenating their bits into a 32-bit integer. A is the sum of all
    bytes in the stream plus one, and B is the sum of the individual values of
    A from each step.

    :param url: file URL
    :returns: Hexified string, padded to 8 values.
    """
    if sig is not None and exp is not None:
        params = [("Signature", sig), ("Expires", exp)]
        url = "{}&{}".format(url, urlencode(params, doseq=True))

    # adler starting value is _not_ 0
    adler = 1

    try:
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            for chunk in r.iter_content(chunk_size):
                adler = zlib.adler32(chunk, adler)
    except Exception as e:
        raise Exception('FATAL - could not get ADLER32 of %s - %s' % (url, e))

    # backflip on 32bit - adler is unsigned in python3
    # if adler < 0:
    #     adler = adler + 2 ** 32

    return str('%08x' % adler)


@hug.post("/md5", map_params={"Signature": "sig", "Expires": "exp"})
def md5(url, sig=None, exp=None):
    """
    Runs the MD5 algorithm (RFC-1321) on the binary content of the file named
    file and returns the hexadecimal digest

    :param url: file URL
    :returns: string of 32 hexadecimal digits
    """
    if sig is not None and exp is not None:
        params = [("Signature", sig), ("Expires", exp)]
        url = "{}&{}".format(url, urlencode(params, doseq=True))

    _md5 = hashlib.md5()
    try:
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            list(map(_md5.update, r.iter_content(chunk_size)))
    except Exception as e:
        raise Exception('FATAL - could not get MD5 of %s - %s' % (url, e))

    return _md5.hexdigest()
